import { createStore } from 'vuex'
import {v4 as uuidv4} from 'uuid'

export default createStore({
  state: {
    todos: [
      { id: 1, text: 'Recolectar las gemas del infinito', completed: false },
      { id: 2, text: 'Gema del alma', completed: true },
      { id: 3, text: 'Gema del Poder', completed: true },
      { id: 4, text: 'Gema de realidad', completed: false },
      { id: 5, text: 'Conseguir Secuaces', completed: false },
    ]
  },
  mutations: {
    toggleTodo(state,id){
      const todoIdx = state.todos.findIndex(todo => todo.id === id)
      state.todos[todoIdx].completed = !state.todos[todoIdx].completed
    },
    createTodo(state, text = ''){
      if (text.length <= 1 ) return 

      state.todos.push({ 
        id:uuidv4(),
        completed:false,
        text
      })
    }
  },
  actions: {
  },
  getters: {
    pendingTodos: (state, getters, rootState) => {
      return state.todos.filter(todo => !todo.completed)
    },
    allTodos: (state, getters, rootState) => {
      return state.todos
    },
    completedTodos: (state, getters, rootState) => {
      return state.todos.filter(todo => todo.completed)
    },
    getTodosByTap: (_, getters) => (tap) => {
      switch (tap) {
        case 'all': return getters.allTodos
        case 'pending': return getters.pendingTodos
        case 'completed': return getters.completedTodos
        default: ''
      }
    }
  },
  modules: {
  }
})
