import { computed, ref } from "vue";
import { useStore } from "vuex";

const useTodos = () => {

  const store = useStore();
  const currentTap = ref('all');

  const pending = computed(() => store.getters["pendingTodos"]);
  const all = computed(() => store.getters["allTodos"]);
  const completed = computed(() => store.getters["completedTodos"]);

  const getTodosByTap = computed(() => store.getters["getTodosByTap"](currentTap.value));

  return {
    pending,
    all,
    completed,
    currentTap,
    getTodosByTap,
    toggleTodo: (id) => store.commit('toggleTodo', id),
    createTodo: (text) => store.commit('createTodo', text)

  }
}

export default useTodos